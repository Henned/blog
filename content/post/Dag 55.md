---
title: Maastricht reiss!
subtitle: Could be worse
tags: [Maastricht]
date: 2018-02-08
---

Donderdag 8 feb

Ontbijt, niet lekker. We zijn begonnen met het brainstormen naar ideeën. Uiteindelijk hadden we verschillende ideeën gecombineerd om aan een concept te komen. Daarna hebben we met de materialen die beschikbaar waren een prototype gemaakt. Eind van de dag gingen de peercoaches langs om de beste concept te vinden. We hadden jammer genoeg niet gewonnen maar een pen en kladblok als prijs kan ik wel missen. Dit keer werd het avondeten niet geregeld voor ons en zijn we zelf op zoek gegaan. Vervolgens zijn we weer uit geweest en weer veel te laat terug naar het hostel gegaan.
