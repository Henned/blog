﻿---
title: Donderdag 12 Oktober
subtitle:  Toneelstuk
tags: [keuzevak, tentamen]
date: 2017-10-12
---


Alweer keuzevak Japans! 
Geleerd over aanwijzingen en meer hiragana.
Dan was het tijd voor tentamen! Ging best wel goed en heb er een goed gevoel over.
Spiekbrief hielp enorm!

![Spiekbrief](https://i.imgur.com/b2e4JFr.png)
