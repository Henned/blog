---
title: Maandag 27 November
subtitle:  Vergadering
tags: [Studiodag, Klassenvertegenwoordiger]
date: 2017-11-27
---


Ik ben begonnen met het maken van de **User Journey**.
Dit heb ik gedaan op papier. 
Vervolgens heb ik Aidan de taak gegeven om dit digitaal vorm te geven.
Hierna heb ik een begin gemaakt aan het ontwerpcriteria.

Ik ben naar de Klassenvertegenwoordigers vergadering gegaan.
Ging goed en over veel dingen gepraat.

![enter image description here](https://i.imgur.com/8OUjOBA.png)