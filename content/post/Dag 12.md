﻿---
title: Woensdag 20 September
subtitle:  Mindmaps..
tags: [studiodag, studiecoaching]
date: 2017-09-20
---


Verder gegaan met het maken van de spelanalyse samen met Jordan.
Dan met het team met elkaar gepraat over de gekozen spel en conclusie.
Vervolgens workshop Creative Technieken gevolgd.
Hier moesten we een thema kiezen en ervan een mindmap creëren over de gekozen thema.

![mindmap](https://i.imgur.com/mp1Upqc.png)

Zodra je klaar was, kreeg je een beoordeling. Vervolgens moest je je beoordeling fotograferen en in je **leerdossier** archiveren.
Dan was het tijd voor studiecoach, besproken over wat een juiste ik profiel is en de opdracht gekregen om een tweede versie van je ik-profiel te maken.
