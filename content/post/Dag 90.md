---
title: Starr's
subtitle: Indesign
tags: [studiodag]
date: 2018-05-30
---

Woensdag 30 Mei

Vandaag de hele dag starr's aangepast. Ik heb mijn starrs verwerkt in Indesign. Ik vind werken met Indesign fijn.
Ik hoop meer Indesign te leren.