---
title: Maastricht reis!
subtitle: Ik haat bussen
tags: [Maastricht]
date: 2018-02-07
---

Woensdag 7 Feb


Vroeg op school. Bus heeft een beetje vertraging dus bijpraten met klasgenoten.
De heenreis duurde ongeveer 2 uurtjes. Eenmaal aangekomen kregen alle studenten de sleutels voor de kamers. Tot mijn schrik moest ik mijn kamer delen met mede CMD’ers uit 1B. Dit had ik liever niet dus heb ik aan Astrid gevraagd om mij een nieuwe kamer te geven. 
Vervolgens moesten we verzamelen in de kantine. Hierbij moeten we onze avatar tekenen en gingen de peercoaches verschillende avatars kiezen om een groepje te maken.  Mijn groepje bestond uit Beau en 4 andere uit andere klassen. Begin was een beetje awkward maar ik kan goed met awkward omgaan.  Ieder groepje kreeg zijn thema en moest een soort van minidesign challenge gaan verrichten. Mijn groep had als doelgroep ZZP’ers. Hier was ik blij mee want het is best wel makkelijk om die te vinden in de stad. We gingen de stad in en hebben geluncht daarna zijn we gaan zoeken naar ZZP’ers. Eenmaal gevonden vroegen we of we ze mochten interviewen voor onze onderzoek. 
Het begon laat te worden en gingen terug naar het hostel. Hier moest je je informatie verwerken in een gebruikersprofiel en journey. Daarna was het avondeten tijd. In de nacht ben ik nog uit geweest met mijn klas en kwam ik veel te laat terug.

![squad](https://i.imgur.com/HrylO8V.png)
