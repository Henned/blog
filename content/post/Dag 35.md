---
title: Woensdag 22 November
subtitle:  Merkanalyse
tags: [Studiodag, workshop]
date: 2017-11-22
---

## Merkanalyse.
Wist niet precies hoe een merkanalyse in elkaar zit.
Ik heb toen instructie document door gelezen en besloten om de template te gebruiken.
Ik heb vervolgens een merkanalyse gemaakt van een concurrent evenementbedrijf.
En ik ben nu ook eens de klassenvertegenwoordiger.

Workshop D-Scrum gevolgd.
Ik wist al veel van scrum door stage dus niet echt veel meegekregen.

![Bob](https://i.imgur.com/YnasoJO.png)