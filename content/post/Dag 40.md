---
title: Maandag 4 December
subtitle: Valideren
tags: [Studiodag]
date: 2017-12-04
---


Merkanalyse gevalideerd bij de vakdocent. Veel feedback ontvangen.
Het ging vooral om dat ik abstracte woorden gebruik en concreter moest zijn.
Ik heb daarna de feedback goed doorgenomen en verwerkt.
Tweede en derde lowfid prototype gemaakt met Aidan.
