---
title: Terugblik
subtitle: Pfew
tags: [terugblik]
date: 2018-06-03
---

Ik heb veel geleerd afgelopen jaar. 
Al hoewel ik in het begin competenties en leerdossier niet leuk vond,
vind ik het nu wel fijn. Het leerdossier helpt me mijn progressie te zien en laat me ook reflecteren.
Ik vind de opleiding erg fijn en hoop in jaar 2 nog veel meer te leren.
