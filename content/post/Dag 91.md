---
title: Expo
subtitle: Winnings
tags: [expo]
date: 2018-06-01
---

Vrijdag 1 Juni

Eerder naar school gegaan want vandaag was er expo.
Ik heb samen met Sico en Joshua onze stand opgemaakt met de high fid's en de posters.
Vervolgens eten gehaald voor bij ons concept. Gepitched voor de opdrachtgevers Joke en Ange. Vervolgens ook aan iemand van de gemeente en vele ouders.
Iedereen was enthousiast over ons concept omdat het helder was. Elske heeft daarna de High Fid gevalideerd. Ons concept werd uiteindelijk gekozen als een van de winnaars. 
