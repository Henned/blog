---
title: Terugblik
subtitle: Einde periode 2
tags: [Kwartaal einde]
date: 2018-01-18
---

Einde kwartaal 2. 
Ik heb veel kunnen leren. Het feed forward van periode 1 was dat ik proactief moest zijn. Dit heb ik gedaan door initia te tonen bij álles. Dit heeft geleid tot een positieve attitude dat ik overbracht bij mijn team. Dit waardeerde ze en is terug te zien in de peerfeedback. 
Ik heb als ontwerper kunnen groeien door deliverables te maken.

Ik heb geleerd dat een gebruikers profiel, interviews en design rationale je ontzettend kan helpen bij het oplossen van de ontwerpopgave. 

Door workshops te volgen heb ik bijv. geleerd dat scrums in de ochtend een geweldige helikopter view geeft van het ontwerpproces.

Tot nu toe ben ik blij met mijn keuze van mijn opleiding en mijn klas.


