﻿---
title: Woensdag 6 September
subtitle: New and improved studiodag
tags: [studiodag, studiecoaching]
date: 2017-09-06
---

Tweede studio dag. Eerst studiecoaching.
Verder met het onderzoek. 
Het internet bleef maar aan en uit gaan dus ik was vrij snel gedemotiveerd. 
Zodra ik een stabiele connectie had, heb ik veel kunnen verrichten maar niet zoveel als ik wou.
Het onderzoek is zo goed als klaar nu en een begin gemaakt met het onderzoek visueel te maken.
Kennis gemaakt met onze studiecoach. Hier moesten we elkaar beter leren kennen.
Poster maken en rollen verdeeld onder onze projectgroepje. 

![All Starz](https://i.imgur.com/1Js6OvZ.png)

Hierna de workshop “blog schrijven” gehad. 
We moeten blijkbaar een blog schrijven met het markdown methode.
Dit vind ik zeer onnodig want uiteindelijk gaan we werken met html dat ik zeer goed beheers.
