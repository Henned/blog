﻿---
title: Maandag 4 September
subtitle: Eerste studiodag
tags: [studiodag]
date: 2017-09-04
---

Eerste studio dag. 
Gemeenschappelijke informatie gekregen van Bob en Elske - **projectdocenten.**
Studio wordt gebruik door 2 klassen, CMD1D en **CMD1E.** *(Mijn klas!)*

Begonnen met het kiezen van een teamcaptain. 
Teamcaptains zorgen ervoor dat de projectgroepjes en de projectdocenten goed met elkaar kunnen communiceren. 
Uiteindelijk is dat Floris geworden. Die ging dan samen met alle andere teamcaptains praten met de projectdocent.


Maandag en woensdag zijn de studio dagen en er zijn workshopmomenten op de maandag, dinsdag, woensdag en vrijdag.
Als eerst hebben we de eerste deliverable kunnen maken als team
Dit bestaat uit het inventarisatie van de team, teamafspraken en gezamenlijk doelen.

![Battlestation](https://i.imgur.com/2n6diIJ.png)

Vervolgens gingen we oefenen met het maken van een onderzoeksrapport.
Ons groepje bestaat uit 6 mensen en we besloten om in groepjes van 2 een onderzoekrapport te maken.
Ieder groepje kiest dan een doelgroep en een Rotterdams thema uit.
Jordan en ik hadden als doelgroep eerstejaars studenten van het opleiding Bouwkunde gekozen.

![Gebouw](https://i.imgur.com/Cs0YMJ0.png)

Informatie hiervan hebben we kunnen vinden op de HR website.
En het thema dat we hadden uitgekozen was architectuur in Rotterdam. 
Als laatst hebben we een teamplanning gemaakt die Ashley dan in een google sheet had verwerkt. 
