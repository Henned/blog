﻿---
title: Woensdag 04 Oktober
subtitle:   Dierendag
tags: [studiedag, studiecoaching, dierendag]
date: 2017-10-04
---


Presentatie gegeven voor alumni ’s en feedback ontvangen.
Feedback was voornamelijk negatief met als ‘core’: “ik snap het niet”.
Zelf snap ik niet wat er niet te begrijpen is en als er op andere niveaus gedacht gaat worden

zoals vragen stellen die niet horen bij het huidige doelgroep.
Maar goed, lekker socializen met medestudenten bij **BYO**. Erg genoten en gelachen.

Vervolgens studiocoach met Gerhard! Veel geleerd over wat de studie in petto heeft en over **STARR(T).**
