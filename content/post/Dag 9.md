﻿---
title: Donderdag 14 September
subtitle:  Ichi, Ni, San, Yon...
tags: [werkcollege, keuzevak]
date: 2017-09-14
---



Weer eens veels te vroeg maar het is tijd voor Japans!
Vandaag hebben we 15 nieuwe characters, tijdsaanduiding en tellen tot 100 geleerd. 5 uur tussenuurtjes... 

![5 uur later](https://i.imgur.com/FuMvphq.png)

Samen met groepje verder werken aan het project want deadline is morgen om 17:00!
Iedereen maakt zn eigen deel van de presentatie en Jiska is bezig met het verbeelden van de app in Xperience design.
