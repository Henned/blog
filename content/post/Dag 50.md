---
title: Woensdag 10 Januari
subtitle: Chaos
tags: [Studiodag]
date: 2018-01-10
---
Ik ben begonnen met mijn klas in te lichten over het klassenvertegenwoordigersverslag en heb daarnaast gevraag waar ze zich aan ergeren. Wat hieruit voort kwam was dat men al het geld voor Maastricht hadden overgemaakt maar dat echter nog niemand weet wat we gaan doen. Dit heb ik teruggekoppeld naar mijn projectdocent en die gaat er verder mee.

Vervolgens ben ik begonnen aan de Design Rationale.
Hierbij heb ik wat tips gevraagd aan mijn teamgenoten om de deliverable verder te perfectioneren.
