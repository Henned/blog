---
title: Maandag 15 Januari
subtitle: Checklist
tags: [Studiodag]
date: 2018-01-15
---

De expo is pas woensdag dus daar maken we niet druk op. 
De rest van de dag heeft iedereen zijn of haar deliverable afgemaakt en ik heb aan mijn leerdossier gewerkt.
