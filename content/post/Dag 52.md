---
title: Woensdag 17 Januari
subtitle: Xpo
tags: [Studiodag, Expo]
date: 2018-01-17
---


Vandaag hoeven we pas om 1 uur te beginnen. Lekker uitgeslapen en naar school gegaan. Op school heb ik samen met mijn team een paar deliverables en poster uitgeprint op A3 formaat om onze stand te versieren. Ik ben gaan eten bij takumi met klasgenoten en weer snel teruggegaan want de expo begon. Expo was leuk. Leuk op iedereen zijn concepten te zien. Aidan heeft gepitcht voor Robin en die heeft de pitch en het eindresultaat beoordeeld.

![Stand](https://i.imgur.com/ae8rehr.jpg)
