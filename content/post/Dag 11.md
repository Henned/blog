﻿---
title: Dinsdag 19 September
subtitle:  Bob is cool
tags: [hoorcollege, keuzevak]
date: 2017-09-19
---




Hoorcollege nummer 3.
Dit keer werd de lectuur gegeven door Bob! Onderwerp dit keer was prototyping.

Bedoeling is dat concepten die je creerd aansluiten op je doelgroep oftewel ze moeten relevant zijn. **(Design the thing right)**
En uiteraard is het wel zo handig als ’t concept doet wat het hoort te doen. **(Design the right thing)** 
Ook hebben we een wat meegekregen over zelf reflectie illusie **(Introspection illusion)**. Hierbij denkt je iets leuk te vinden door het jezelf te overtuigen met je eigen inkijk.

5 uur later keuzevak illustrator gevolgd. We leerde de basics van layers maken maar omdat ik dat al gewend ben door photoshop had ik het snel door.
