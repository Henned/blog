﻿---
title: Maandag 18 September
subtitle:  Hallo iteratie 2
tags: [studiodag, presenteren, kill your darling]
date: 2017-09-18
---


Begonnen met de presentatie, om eerlijk te zijn helemaal niet goed voorbereid ervoor.
Presentatie was toch nog goed gelukt naar mijn impressie. En het feedback was wat minder.
Met als kernfeedback van Elske: 
### “Where’s the fun”. 

Briefing nummer 2 is **Kill Your Darling.**

Dit wil zeggen gooi *alles* weg en begin *weer* opnieuw maar nu met *meer* kennis.
We behouden het thema en doelgroep en gaan ons nu verdiepen met het analyseren van spellen.
Puzzel spellen om precies te zijn want cmd’ers houden van puzzelen.
Jordan en ik gaan ons verdiepen in Monument Valley. 
Een heel leuk puzzel spel die vele awards heeft gewonnen.
