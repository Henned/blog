---
title: Dinsdag 14 November
subtitle:  Kick-off
tags: [Kickoff]
date: 2017-11-14
---




Kickoff Kwartaal 2.
Het kick-off gebeurde in Paard in Den Haag.
Hier werden we verwelkomd en mochten een presentatie en filmpje volgen.
Hierdoor was de thema en de planning van kwartaal 2 duidelijk.
Bedoeling is dat we eenmalige bezoekers van Paard vaker kunnen terug laten komen.
