---
title: Testen
subtitle: Greenhouse
tags: [studiodag]
date: 2018-05-23
---

Maandag 23 mei

Begonnen met het testen van de concept in Green House. 
In de green house nam ik gewoon plaats en observeerde ik de keuzes die de studenten maakte in de pauze.
Ik hoopte de keuzes te kunnen beïnvloeden door middel van de infocards.
Ik verdeelde mijn pauze in twee x 30 minuten. De eerste half uur had ik geen gebruik gemaakt van het concept. Ik noteerde alle keuzes die de studenten namen.
Testrapportage gemaakt en gevalideerd.