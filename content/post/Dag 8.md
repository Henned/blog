﻿---
title: Woensdag 13 September
subtitle: Ik-profiel? But why tho? 
tags: [studiodag, studiecoaching, workshop]
date: 2017-09-13
---



Met andere groepjes feedback uitgewisseld.
Erg leuk om te zien wat anderen hebben gemaakt,
En wat ze van ons project vinden.
Vervolgens feedback opgeschreven. Evalueren en reflecteren!

Daarna de workshop Moodboards maken gevolgd.
Hierbij geleerd wat voor een doeleinde een moodboard kan hebben.
Dit moodboard moest gemaakt worden mbv tijdschriften.
Heel veel knippen en lijmen... maar ik was wel tevreden met het eindresultaat.

Dan was het tijd voor een band vormen met de studiecoach.
Weer rollen uitgewisseld binnen ons groepje en vervolgens opgeschreven wat onze strengths, weaknesses, opportunities en threats zijn.

![ik-profiel](https://i.imgur.com/1rqQ1xJ.png)

Vervolgens een eerste begin gemaakt aan het ik-profiel.
Dit gaan we later verder visueel uitbreiden.

![ik-profiel](https://i.imgur.com/G0Rghz5.png?2)
