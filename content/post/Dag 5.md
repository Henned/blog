﻿---
title: Donderdag 7 September
subtitle: Keuzevak?!
tags: [keuzevak, japans, werkcollege]
date: 2017-09-07
---

**8:30 beginnen.** Uiteraard wat moeite gehad om wakker te worden.
Ik heb besloten om mijn keuzevak Japans toch maar te volgen ook al hoeven eerstejaars *geen* keuzevak te doen in Blok 1.
Klas van 20 studenten, meeste tweedejaars.
Japans 'sensee' is vriendelijk en behulpzaam. Geleerd hoe we onszelf kunnen voorstellen in Japans.
Ook hebben we wat Hiragana geleerd. Het moderne Japanse tekenschrift. 
Vervolgens afgesproken met mijn projectgroepje en een ‘battle plan’ gecreëerd voor maandag.

Maandag gaan we het papieren prototype creëren.
Hierna werkcollege gevolgd. Tijdens het college moesten we een willekeurige, correcte ontwerpproces uit onze ervaring uitwerken.
Daarna kozen we eentje uit van onze groepje, dit dan verder uitgewerkt en gepresenteerd. 
