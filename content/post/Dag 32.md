---
title: Nieuwe Challenge!
subtitle:  Nieuw groepje?
tags: [Studiodag]
date: 2017-11-15
---


# Team Toxic Waste

Vandaag zijn we begonnen met afscheid nemen van je eigen groep.
Eigenlijk waren we allemaal al mentaal voorbereid ervoor maar toch nog wat heimwee.
Elske had een aantal plaatjes opgehangen rond de studio.
De plaatjes functioneerde als selector van je rol.
Als je een 'denker' was ging je bij het plaatje "denker" staan enz.
Vervolgens moest je van je reflecterende rol iemand zoeken om een duo of trio te vormen.
En vervolgens een duo of trio te zoeken. 

And thus.. Mijn nieuwe projectgroep was gecreerd.
Mijn teamleden zijn: 

Mark
Leila
Jascha
Aidan
Hannad

Hierna gingen we gelijk aan de slag met kennis maken.
Teamafspraken en leerdoelen vastgesteld en jaarwijzer bestuderen.
Daarna gingen we onze doelgroep bestuderen.
"The selector" doelgroep met leeftijd tussen de 28 en 38 jaar oud.
