---
title: Donderdag 31 Augustus
subtitle: Nu al een hoorcollege?!
tags: [hoorcollege, eng, lopen]
date: 2017-08-31
---



Het is 10:50 in de ochtend. **Te laat.** Treinstoring, wat fijn.
Gewoon met een positieve gedachte naar binnen en het hoorcollege gaan volgen.
Tijdens het hoorcollege *“Games”* werd ons al de briefing van het eerste design challenge aangegeven. 
Hierin staat alles vermeld dat ik nodig zal hebben voor het maken van de eerste iteratie en de deliverables.
Een van deze deliverable is het maken van een individuele blog.
Hierin moet ik mijn ontwerpproces, vaardigenheden, bevindingen van aantekeningen enz. vast gaan leggen.

![Hoorcollege](https://i.imgur.com/HLdIjfd.jpg)

In dit hoorcollege hebben we geleerd *“Wat een game is”* en wat een game een game maakt. 
Uiteindelijk kwamen we uit tot de conclusie dat games moeilijk te omschrijven zijn.
Games zijn een ervaring en ervaring moet een betekenis **(context)** hebben.
Door de relevantie wil men de ervaring meemaken.

Het Game Design regel nummer 1 is “Gameplay comes first!”
Een game bestaat uit: een doel, regels en keuzes. Als een game voldoet aan deze eisen dan is er gameplay.

Een spel moet een doel hebben. Er moet dus Meaningful Play zijn.
Meaningful Play maakt de games leuk.

Regels bestaan alleen in de “Magic Circle”. 
Dit wil zeggen regels kunnen alleen toegepast worden in de “Magic circle” en niet in realiteit. 
Keuzes geeft de speler vrijheid en kan hij  ,indien mogelijk, ongelimiteerd het spel spelen zoals hij dat wilt.

Na het hoorcollege hebben we een Rotterdam tour gelopen. Hierbij moesten we monumenten en HR medewerkers vinden.

![Tour](https://i.imgur.com/v20NIiB.jpg)