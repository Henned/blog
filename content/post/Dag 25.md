﻿---
title: Woensdag 11 Oktober
subtitle:  Toneelstuk
tags: [studiedag, studiocoaching]
date: 2017-10-11
---


Hard aan slag met iteratie 3. 
Project aanpassen en verbeteren.
Ik maak een aantal plaatjes die we kunnen gebruiken voor onze expostand.
Daarna veel geleerd over het **leerdossier** en **STARR.**

Hierbij ook een ‘toneelstuk’ gespeeld met Gerhard om een beter inzicht te krijgen hoe een STARR  gesprek verloopt.
Vervolgens ook feedback gekregen

![feedback](https://i.imgur.com/fu9gfYb.png)

