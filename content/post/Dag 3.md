﻿---
title: Dinsdag 5 September
subtitle: Eerste hoorcollege van Design Theorie
tags: [hoorcollege]
date: 2017-09-05
---

Eerste hoorcollege **Design Theorie**.
Hier werd ons uitgelegd wat we kunnen verwachten van het eerste kwartaal.
Design Theorie gaat ons uiteindelijk 

> 4 studiepunten

 opleveren. 
Opgebouwd uit werk- en hoorcolleges en zelfstudie.
Wat ik voornamelijk heb geleerd van deze eerste hoorcollege is het antwoord op de vraag:
### *“What is design?”*
Het antwoord hierop is eigenlijk dat het concept “design” moeilijk uitleggen is.
Ook hebben we geleerd hoe een ontwerpproces in theorie correct is.
Maar dit veranderd continu want er is altijd een verandering dat plaats vindt.
Hierdoor is het ontwerpproces niet lineair. 
Op het rooster stond ook een workshop ingeroosterd maar ging niet door.
Vervolgens met mijn projectgroepje een bakkie koffie wezen halen.
