---
title: Dinsdag 9 December
subtitle: Choice
tags: [hoorcollege]
date: 2018-01-09
---

Besloten om thuis te blijven en opnieuw alle hoorcolleges te kijken. 
De tentamen is donderdag al en omdat de hoorcollege al online stond vond ik het niet nodig om nog naar school te gaan want ik kan de reistijd beter omruilen voor een hoorcollege die ik kan afkijken.