---
title: Brainstormen
subtitle: Van negatief naar positief
tags: [studiodag]
date: 2018-04-25
---

Woensdag 25 april

Ik heb samen met mijn team gebrainstormed. 
We willen een concept bedenken. Hierbij hebben we verschillende brainstorm technieken gebruikt.
Zoals kettingsrecatie, negatieve brainstorming en woordenspin. Leuke ideen uitgekomen.