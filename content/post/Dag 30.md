﻿---
title: Back to the future?
subtitle:  or back to the past?
tags: [Terugblik]
date: 2017-10-26
---


# Een blik terug.

*Wat heb ik geleerd?*
Pff waar zal ik mee ik beginnen? 
Dat ik zoveel heb opgenomen in 2 maanden tijd. *Wie had dat verwacht.*
Ja, ik heb zoveel geleerd maar ook kennisgemaakt met allemaal fun people.
Zo heb ik creatieve technieken geleerd zoals mindmapping en het toepassen van de double diamond methode.

Ik heb mezelf wijs gemaakt dat ik zelf beslis wat ik leer en ook hoe ik dit leer.
In vergelijking met mijn vorige opleiding waar alles voor me werd geschoven met de mentaliteit zo van:
"Doe dit zo en dat zo!' is dit opleiding veeeeeel vrijer. En dat vind ik prettig.

Ik ben erg tevreden over de manier waarop iedereen in me klas met elkaar omgaan alhoewel we nog wel een beetje afstandelijk van elkaar zijn is de sfeer wel al goed.

*Wat verwacht ik van kwartaal 2?*
Ik verwacht dat ik in het volgende kwartaal nog meer zal gaan leren en hopelijk de kans krijgen om mijn klas beter te leren kennen.
